This was originally posted on https://ramble.karrth.com/archives/170. It is no longer available outside of the Internet Archive, so it's being archived here for easier discovery.

-------------------

# Subsonic 4.x and SSL certificates on Debian/Ubuntu

One of my favorite applications of late is Subsonic. It allows you to stream all of your media over a nice web interface, or to your mobile devices. I wanted to setup my installation with a proper SSL certificate, since I use my own subdomain for it.

EDIT: This has worked for me with Subsonic 4.8 and 4.9

## Prerequisites
- This only worked for me when running openjdk-7-jre, not 6. You can install it with:

    `sudo apt-get install openjdk-7-jre`

- You need the jar tool if you�re going to update the jar file, but this is an optional step. If you need this package, install:

    `sudo apt-get install openjdk-7-jdk`

    Or you can use the zip or 7z utility, since jars are zips

## The Setup
1. Stop Subsonic:

    `service subsonic stop`

2. Backup your subsonic.sh file:

    `cp /usr/share/subsonic/subsonic.sh /usr/share/subsonic/subsonic.sh.backup`

3. Edit `/usr/share/subsonic.sh` and set SUBSONIC_HTTPS_PORT= to the port you want to use. 443 is the default https port, but may be blocked by your ISP if you are hosting from home. If you want to disable http, you can set the SUBSONIC_HTTP_PORT=0, but this is not necessary as it will redirect to the HTTPS port if enabled.

4. Acquire a [self generated](http://www.openssl.org/docs/HOWTO/certificates.txt) or [purchased](https://www.ssl2buy.com/) SSL certificate. The one that comes with Subsonic is a wildcard certificate for *.subsonic.org. This works best if you utilize their DNS service and use a subsonic.org subdomain to reach your server. You should now have a .key and a .crt file to work with

5. Convert your .crt file to a PKCS12 format. When prompted give the password �subsonic�:

    `openssl pkcs12 -in mydomain.crt -inkey mydomain.key -export -out subsonic.pkcs12`

6. Create the keystore. When prompted, give the passwords �subsonic�:

    `keytool -importkeystore -srckeystore subsonic.pkcs12 -destkeystore subsonic.keystore -srcstoretype PKCS12 -srcalias 1 -destalias subsonic`

7. Tell Subsonic to use your keystore one of two ways:

    1. Backup the jar file, overwrite the original keystore and verify the permissions:
	
        `cp /usr/share/subsonic/subsonic-booter-jar-with-dependencies.jar /usr/share/subsonic/subsonic-booter-jar-with-dependencies.jar.backup`

        `jar uf /usr/share/subsonic/subsonic-booter-jar-with-dependencies.jar subsonic.keystore`

        `chmod 744 /usr/share/subsonic/subsonic-booter-jar-with-dependencies.jar`

        If you want to use 7zip or zip, replace the jar command with:

        `zip -u /usr/share/subsonic/subsonic-booter-jar-with-dependencies.jar subsonic.keystore`

        or

        `7z u /usr/share/subsonic/subsonic-booter-jar-with-dependencies.jar subsonic.keystore`

        OR instead of replacing the file:

    2. Save it to Subsonic�s home and refer to it with the configuration:

        1. Copy the keystore to Subsonic�s home and verify ownership/permissions:

            `cp subsonic.keystore /var/subsonic/`

            `chown subsonic:root /var/subsonic/subsonic.keystore`

            `chmod 744 /var/subsonic/subsonic.keystore`

        2. Edit /usr/share/subsonic/subsonic.sh and add these additional lines at line 123, after `-Djava.awt.headless=true \` and before `-verbose:gc \`:

            `-Dsubsonic.ssl.keystore=/var/subsonic/subsonic.keystore \`

            `-Dsubsonic.ssl.password=subsonic \`

8. Start Subsonic:

    `service subsonic start`

That�s it! Make sure you login to confirm it�s working, and check your certificate at the top. If something fails, restore those backups! To check for errors, watch `/var/subsonic/subsonic_sh.log`. It�s re-created every time Subsonic starts up.

##### Tags: computers, debian, java, linux, music, ssl, subsonic
---
##### This entry was posted	on Friday, February 7th, 2014 at 09:10	and is filed under computers, music. You can follow any responses to this entry through the RSS 2.0 feed. You can leave a response, or trackback from your own site.
---

### One Response to �Subsonic 4.x and SSL certificates on Debian/Ubuntu�
------------------------------------------------------------------------
#### ITBadger Says:
##### December 30th, 2014 at 01:40
Thanks very much for this �most importantly for providing another way for subsonic to access the keystore, other than to overwrite the existing one by injecting directly into the jar file.

One point to note �as I created my certificate wt my own CA and then imported it as a PKCS12 file directly ..its alias wasnt �1�

So for any who might be interested, I grabbed the alias of the certificate by running:

`keytool -list -keystore /home/subsonicsvc/CERTNAME.pkcs12 -storetype pkcs12`

I was then able to follow the above instructions

Thanks again :D
